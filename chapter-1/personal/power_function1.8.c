#include <stdio.h>
//program to experiment with functions. Declares a function to do exponentiation
int power(int base, int exp);

main()
{
    int i;

    for(i = 0; i < 10; ++i)
        printf("2 and - 3 to the power of %d: %d and %d\n", i, power(2, i), power(-3, i));
    return 0;
}

int power(int base, int exp){
    int product;

    for(product = 1; exp > 0; --exp)
        product = product * base;
    return product;
}
