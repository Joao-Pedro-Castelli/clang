#include <stdio.h>
#define MAXLEN 1000

int fgetline(char string[], int limit); //apparently getline is now part of glibc, so just break K&R i guess
void copy(char to[], char from[]);

//print the longest line inputed
main()
{
    int lenght, biggest; //length of the current line and of the biggest one
    char currentline[MAXLEN];
    char longestline[MAXLEN];
    biggest = 0;

    while((lenght = fgetline(currentline, MAXLEN)) != 0){
        if(lenght > biggest){
            copy(longestline, currentline);
            biggest = lenght;
        }
    }
    if(biggest > 0)
        printf("%s\n", longestline);
    return 0;
}

//receives an empty char array and its lenght and puts a line of input in it
int fgetline(char string[], int limit)
{
    int i, chara;

    for(i = 0; i < limit - 1 && (chara = getchar()) != EOF && chara != '\n'; ++i){
        string[i] = chara;
    }
    if(chara == '\n'){
        string[i] = '\n';
        ++i;
    }
    string[i] = '\0';
    return i;
}

//copies one char array to another empty one
void copy(char to[], char from[])
{
    int i;
    i = 0;

    while((to[i] = from[i]) != '\0')
        ++i;
    to[i] = '\0';
}
