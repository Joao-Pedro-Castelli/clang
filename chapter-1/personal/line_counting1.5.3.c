#include <stdio.h>

main() 
{
    int chara, numlines;
    numlines = 0;
    while((chara = getchar()) != EOF) {
        if (chara == '\n'){
            ++numlines;
        }
    }
    printf("The number of lines in input is %d\n", numlines);
}
