#include <stdio.h>
#define IN 1
#define OUT 0

main() 
{
    /*if the last char of the input text stream was a word or not, char, last char, new=>(line, char and word) */
    int state, chara, lastchara, newline, newchara, newword;

    newline = newchara = newword = 0;
    state = OUT;

    while ((chara = getchar()) != EOF) {
        ++newchara;
        if (chara == '\n')
            ++newline;
        if (chara == '\n' || chara == '\t' || chara == ' ')
            state = OUT;
        else if (state == OUT){
            ++newword;
            state = IN;
        }
        lastchara = chara;
    }
    /*if input doens't end with \n,even though that should be the standard, it won't count the last line*/
    if (lastchara != '\n' && newchara > 0)
        ++newline;
    printf("Number of: lines => %d  characters => %d  words => %d\n", newline, newchara, newword);
}
