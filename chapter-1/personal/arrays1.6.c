#include <stdio.h>

/*count the number of times there is a digit, a space or something else (other) in the input stream */
#define NUMDIGITS 10 /* number of digits. It could be something other than decimal */
main()
{
    int numdigits[NUMDIGITS];
    int numspaces, nothers, chara, i;

    for(i = 0; i < NUMDIGITS; ++i){
        numdigits[i] = 0;
    }

    while((chara = getchar()) != EOF){
        if('9' >= chara && chara >= '0')
            ++numdigits[chara - '0'];
        else if(chara == ' ' || chara == '\t')
            ++numspaces;
        else
            ++nothers;
    }
    
    printf("Digits: ");
    for(i = 0; i < NUMDIGITS; ++i)
        printf("%d = %d | ", i, numdigits[i]);
    printf("Spaces (or tabs) = %d and others = %d\n", numspaces, nothers);
}
