#include <stdio.h>
#define IN 1
#define OUT 0

/* Program that prints its input one word per line */
main() 
{
    int chara, state;
    state = OUT;

    while ((chara = getchar()) != EOF){
        if (chara != ' ' && chara != '\n' && chara != '\t'){
            if (state == OUT){
                putchar(chara);
                state = IN;
            } else
                putchar(chara);
        } else
            putchar('\n');
            state = OUT;
    }
}
