#include <stdio.h>
#define MAXLENGHT 1000
#define LENGHTCAP 80

int fgetline(char string[], int limit);

main()
{
    int lenght;
    char currentLine[MAXLENGHT];
    
    printf("These lines are bigger than %d characters:\n", LENGHTCAP);
    while((lenght = fgetline(currentLine, MAXLENGHT)) != 0){
        if(lenght > LENGHTCAP)
            printf("%s\n", currentLine);
    }
    return 0;
}

int fgetline(char string[], int limit)
{
    int i, chara;

    for(i = 0; i < limit - 1 && (chara = getchar()) != EOF && chara != '\n'; ++i)
        string[i] = chara;

    if(chara == '\n'){
        string[i] = '\n';
        ++i;
    }
    string[i] = '\0';
    return i;
}
