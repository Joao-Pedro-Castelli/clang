#include <stdio.h>
#define NUMCHAR 1000
int fgetline(char string[], int limit);
void reverseString(char string[], int length);
//takes lines of input and reverse each one separately
main()
{
    int length;
    char currentLine[NUMCHAR];

    while((length = fgetline(currentLine, NUMCHAR)) != 0){
        reverseString(currentLine, length);
        printf("\nLine reversed: %s\n", currentLine);
    }
    return 0;
}

int fgetline(char string[], int limit)
{
    int i, chara;

    for(i = 0; i < limit - 1 && (chara = getchar()) != EOF && chara != '\n'; ++i)
        string[i] = chara;

    if(chara == '\n'){
        string[i] = '\n';
        ++i;
    }
    string[i] = '\0';
    return i;
}

void reverseString(char string[], int length){
    int i, temp;
    //don't want to reverse \0 and \n
    if(string[length - 1] == 'n')
        --length;
    --length;

    for(i = 0; i < length; ++i){
        temp = string[length];
        string[length] = string[i];
        string[i] = temp;
        --length;
    }
}
