#include <stdio.h>

/* fahrenheit to celsius conversion table
   pretty cool exercise */

main()
{
    
    float fahrenheit, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;
    fahrenheit = lower;
    
    printf("-----Temperature conversion table-----\n\n       Fahrenheit  =>  Celsius\n");
    while(fahrenheit <= upper){
        celsius = 5.0/9.0 * (fahrenheit - 32.0);
        printf("%13.0f %15.1f\n", fahrenheit, celsius);
        fahrenheit = fahrenheit + step;
    }
}
