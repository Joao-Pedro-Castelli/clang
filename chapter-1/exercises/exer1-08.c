#include <stdio.h>

main() 
{
    int chara, numblanks, numtabs, numnewlines;
    numblanks, numtabs, numnewlines = 0;

    while((chara = getchar()) != EOF){
        if (chara == '\n'){
            ++numnewlines;
        }else if (chara == '\t'){
            ++numtabs;
        }else if (chara == ' '){
            ++numblanks;
        }
    }
    printf("The number of blanks is %d, of tabs is %d and of newlines is %d\n", numblanks, numtabs, numnewlines);
}
