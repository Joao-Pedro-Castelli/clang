#include <stdio.h>
#define MAXLEN 1000

int fgetline(char string[], int limit); //apparently getline is now part of glibc, so just break K&R i guess
void copy(char to[], char from[]);

//print the longest line inputed
main()
{
    int lengthOfLine, biggest, chara; //length of the current line and of the biggest one. and a char that getschar
    char currentLine[MAXLEN];
    char longestLine[MAXLEN];
    biggest = 0;

    while((lengthOfLine = fgetline(currentLine, MAXLEN)) != 0){
        if(currentLine[lengthOfLine - 1] != '\n'){
            while((chara = getchar()) != EOF && chara != '\n'){
                ++lengthOfLine;
            }
            if(chara == '\n')
                ++lengthOfLine;
        }
        if(lengthOfLine > biggest){
            copy(longestLine, currentLine);
            biggest = lengthOfLine;
        }
    }
    if(biggest > 0)
        printf("\n\nThe biggest line (below) had %d characters:\n%s\n", biggest, longestLine);
    return 0;
}

//receives an empty char array and its len and puts a line of input in it
int fgetline(char string[], int limit)
{
    int i, c;

    for(i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i){
        string[i] = c;
    }
    if(c == '\n'){
        string[i] = '\n';
        ++i;
    }
    string[i] = '\0';
    return i;
}

//copies one char array to another empty one
void copy(char to[], char from[])
{
    int i;
    i = 0;

    while((to[i] = from[i]) != '\0')
        ++i;
    to[i] = '\0';
}
