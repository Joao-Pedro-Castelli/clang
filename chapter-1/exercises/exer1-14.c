#include <stdio.h>
/* histogram of frequencies of different characters (I will consider these to be letters, white space, digits, new lines or others.) */
#define LETTER 0
#define WHITESPACE 1
#define DIGITS 2
#define NEWLINE 3
#define OTHERS 4
#define NUMCHARA 5 /* number of different chars tracked, in case it's added more */

main()
{
    float numOcurrences[NUMCHARA];
    float freq[NUMCHARA];
    char column[NUMCHARA];
    int chara, i, totchar;
    float i2;
    totchar = 0;

    for(i = 0; i < NUMCHARA; ++i){
        numOcurrences[i] = 0.0;
        freq[i] = 0.0;
        column[i] = ' ';
    }

    while((chara = getchar()) != EOF){
        if(('Z' >= chara && chara >='A') || ('z' >= chara && chara >= 'a'))
            ++numOcurrences[LETTER];
        else if (chara == ' ' || chara == '\t')
            ++numOcurrences[WHITESPACE];
        else if('9' >= chara && chara >= '0')
            ++numOcurrences[DIGITS];
        else if(chara == '\n')
            ++numOcurrences[NEWLINE];
        else
            ++numOcurrences[OTHERS];
        ++totchar;
    }
    
    for(i = 0; i < NUMCHARA; ++i)
        freq[i] = numOcurrences[i] / totchar;

    printf("Frequency of different types of characters:\n");
    for(i2 = 1.0; i2 >= 0.0; i2 -= 0.1){
        printf("%.1f ", i2);
        for(i = 0; i < NUMCHARA; ++i){
            if(freq[i] >= i2 - 0.05)
                column[i] = '|';
            printf("      %c       ", column[i]);
        }
        printf("\n");
    }
    printf("       Letters   | White space |    Digits   |  New lines  |    Others   \n");
}
