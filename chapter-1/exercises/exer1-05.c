#include <stdio.h>

int main()
{
    float fahrenheit;
    printf("-----Temperature conversion table-----\n\n       Fahrenheit  =>  Celsius\n");
    for(fahrenheit = 300; fahrenheit>=0; fahrenheit = fahrenheit - 20){
        printf("%13.0f %15.1f\n", fahrenheit, 5.0/9.0 * (fahrenheit - 32));
    }
    return 0;
}
