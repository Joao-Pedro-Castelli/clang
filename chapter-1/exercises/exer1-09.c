#include <stdio.h>

main() 
{
    /*character and last character*/
    int chara, lastchara;
    lastchara = 0;
    while((chara = getchar()) != EOF){
        if (chara != ' ' || lastchara != ' '){
            putchar(chara);
        }
        lastchara = chara;
    }
}
