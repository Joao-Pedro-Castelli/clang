#include <stdio.h>
#define NUMCHARA 1000

int fgetline(char string[], int limit);
int rmTrailingBlanks(char string[], int length);

main()
{
    int length, newLength;
    char currentLine[NUMCHARA];

    while((length = fgetline(currentLine, NUMCHARA)) != 0){
        printf("length: %d\n", length);
        newLength = rmTrailingBlanks(currentLine, length);
        if(newLength > 0){
            printf("New length: %d\n", newLength);
            printf("\n%s\n", currentLine);
        }
    }
    return 0;
}

int fgetline(char string[], int limit)
{
    int i, chara;

    for(i = 0; i < limit - 1 && (chara = getchar()) != EOF && chara != '\n'; ++i)
        string[i] = chara;

    if(chara == '\n'){
        string[i] = '\n';
        i++;
    }
    string[i] = '\0';
    return i;
}

int rmTrailingBlanks(char string[], int length)
{
    if(string[0] == '\0' || string[0] == '\n')
        return 0;

    int i;
    i = length;
    if(string[length - 1] == '\n')
        --i;

    for(--i; i >= 0 && (string[i] == ' ' || string[i] == '\t'); --i){
        ;
    }
    if(i == -1)
        return 0;

    if(string[length - 1] == '\n'){
        string[i + 1] = '\n';
        string[i + 2] = '\0';
        return i + 2;
    } else{
        string[i + 1] = '\0';
        return i + 1;
    }
}
