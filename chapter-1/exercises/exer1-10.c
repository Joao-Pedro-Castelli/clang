#include <stdio.h>

main() 
{
    int chara;

    while((chara = getchar()) != EOF){
        if (chara == '\t')
            printf("\\t");
        else if (chara == '\b')
            printf("\\b");
        else if (chara == '\\')
            printf("\\\\");
        else
            putchar(chara);
    }
}
