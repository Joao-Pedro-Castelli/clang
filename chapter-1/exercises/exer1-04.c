#include <stdio.h>

/* Celsius to Fahreheit conversion table
   another pretty cool exercise */

main()
{
    
    float fahrenheit, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;
    celsius = lower;
    
    printf("-----Temperature conversion table-----\n\n       Celsius  =>  Fahrenheit\n");
    while(celsius <= upper){
        fahrenheit = celsius * 9.0/5.0 + 32.0;
        printf("%13.0f %15.1f\n", celsius, fahrenheit);
        celsius = celsius + step;
    }
}
