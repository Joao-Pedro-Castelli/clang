#include <stdio.h>

/*histogram of lengths of word */
#define BIGESTWORD 15
main()
{
    int lengthOfWord[BIGESTWORD];
    char column[BIGESTWORD];
    int chara, i, i2, biggest, curlen; /* current length, when state is IN */
    biggest = 0;
    curlen = 0;

    for(i = 0; i < BIGESTWORD; ++i){
        lengthOfWord[i] = 0;
        column[i] = ' ';
    }


    while((chara = getchar()) != EOF){
        if(('z' >= chara && chara >= 'a') || ('Z' >= chara && chara >= 'A') || chara == '-'){
            ++curlen;

        } else if(curlen != 0 && curlen <= BIGESTWORD){ /* the 0 position in the array will be for the word of lenght 1 */
            ++lengthOfWord[curlen - 1];
            if(lengthOfWord[curlen - 1] > biggest)
                biggest = lengthOfWord[curlen - 1]; /* need to know what's the largest column to print vertically */
            curlen = 0;
        }
    }

    for(i = biggest; i > 0; --i){ /* vertical */
        if(i > 9)
            printf("                %d ", i);
        else
            printf("                 %d ", i);

        for(i2 = 0; i2 < BIGESTWORD; ++i2){ /* horizontal */
            if(lengthOfWord[i2] - i >= 0){
                column[i2] = '|';
            }
            printf("%c   ", column[i2]);
            if(i2 > 9 - 1)
                printf(" "); /* correction for there being two digits in the x-axis */
            
        }
        printf("\n");
    }
    printf("Words with length: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 \n");
}
